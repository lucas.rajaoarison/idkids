import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router';

import { routes } from './app.routes';
import {provideEnvironmentNgxCurrency} from "ngx-currency";

export const CustomCurrencyMaskConfig = {
  align: 'right',
  allowNegative: false,
  allowZero: true,
  decimal: ',',
  precision: 0,
  prefix: ' ',
  suffix: '',
  thousands: ' ',
  nullable: true
};


export const appConfig: ApplicationConfig = {
  providers: [
    provideRouter(routes),
    provideEnvironmentNgxCurrency(CustomCurrencyMaskConfig),
  ]
};
