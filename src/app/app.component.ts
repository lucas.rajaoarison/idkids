import {Component, inject, OnInit} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {FormArray, FormBuilder, FormGroup, ReactiveFormsModule, Validators} from "@angular/forms";
import {CommonModule, NgForOf} from "@angular/common";
import {NgxCurrencyDirective} from "ngx-currency";

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet, ReactiveFormsModule, CommonModule, NgxCurrencyDirective],
  templateUrl: './app.component.html',
  styleUrl: './app.component.scss'
})
export class AppComponent implements OnInit{

  priceTagForm!: FormGroup;

  private fb = inject(FormBuilder);



  ngOnInit(): void {
    this.priceTagForm  = this.fb.group({
      title: ['', Validators.required],
      articles: this.fb.array([
        this.fb.group({
          item: [''],
          price: ['']
        })
      ])
    });
  }


  get dynamicFields() {
    return this.priceTagForm!.get('articles')! as FormArray;
  }

  addField() {
    if (this.dynamicFields.length < 6) {
      this.dynamicFields.push(
        this.fb.group({
          item: [''],
          price: ['']
        })
      )
    }
  }

  removeField(index: number) {
    if (this.dynamicFields.length > 1) {
      this.dynamicFields.removeAt(index);
    }
  }

  onSubmit() {
      console.log(this.priceTagForm.value);
      let dataToSave = this.priceTagForm.value;
      /* DE ETO NO ATSIPY ANY AM BACKEND ILAY DATA */
  }

}
